﻿namespace EchoBot.Profile
{
    public class UserProfile
    {
        private string _resultText = null;
        public string ResultText
        {
            get => _resultText;
            set => _resultText = value;
        }
    }
}