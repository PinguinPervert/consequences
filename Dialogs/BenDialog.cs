﻿using System.Threading;
using System.Threading.Tasks;
using EchoBot.LoopDialogs;
using EchoBot.MyCutbacks;
using EchoBot.Profile;
using EchoBot.Resources;
using Microsoft.Bot.Builder.Dialogs;

namespace EchoBot.Dialogs
{
    public class BenDialog : CancelDialog
    {
        public BenDialog() : base(nameof(BenDialog))
        {
            AddDialog(new MarshallSecondDialog());
            AddDialog(new CallDialog());
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                BeginBenDialogStepAsync,
                DrinkStepAsync,
                WatchStepAsync,
                CallSomeoneStepAsync,
                OfferStepAsync,
                GoingMarshallSecondDialogStepAsync,
            }));
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> BeginBenDialogStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            if (Cutbacks.HasKey("went", "Домой"))
            {
                string text2 = "«Наконец эти лекции на сегодня закончились. Думаю, еще можно заглянуть в лабораторию»";
                return await Cutbacks.Prompt(stepContext, cancellationToken, text2, "В лабораторию");
            }

            await Cutbacks.SendText(Replicas.list[8], stepContext, cancellationToken);
            string text = "«Наконец эти лекции на сегодня закончились. Думаю, еще можно заглянуть в лабораторию»";
            await Cutbacks.SendText(text, stepContext, cancellationToken);
            await Cutbacks.SendImages(ImageURLs.list[4], stepContext, cancellationToken);
            return await Cutbacks.Prompt(stepContext, cancellationToken, "Куда пойти?", "Домой", "В лабораторию");
        }

        private async Task<DialogTurnResult> DrinkStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("went", stepContext);
            CheckLog.AddKey(stepContext.Values);
            if (Cutbacks.Equals("went", "Домой", stepContext))
                return await stepContext.ReplaceDialogAsync(nameof(BenDialog), null, cancellationToken);
            await Cutbacks.SendText(Replicas.list[9], stepContext, cancellationToken);
            return await Cutbacks.Prompt(stepContext, cancellationToken, "Что лучше заварить?", "Кофе", "Чай");
        }

        private async Task<DialogTurnResult> WatchStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("drink", stepContext);
            CheckLog.AddKey(stepContext.Values);
            string text = $"Бэн заваривает {stepContext.Values["drink"]} и решает посмотреть WeTube.";
            await Cutbacks.SendText(text, stepContext, cancellationToken);
            await Cutbacks.SendImages(ImageURLs.list[5], stepContext, cancellationToken);
            return await Cutbacks.Prompt(stepContext, cancellationToken, "Какой канал выбрать?",
                "Daily Post", "National Geographic");
        }

        private async Task<DialogTurnResult> CallSomeoneStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("channel", stepContext);
            CheckLog.AddKey(stepContext.Values);
            if (Cutbacks.Equals("channel", "National Geographic", stepContext))
                return await stepContext.EndDialogAsync(new UserProfile {ResultText = "National Geographic"},
                    cancellationToken);
            await Cutbacks.SendText(Replicas.list[10], stepContext, cancellationToken);
            return await stepContext.BeginDialogAsync(nameof(CallDialog), null, cancellationToken);
        }

        private async Task<DialogTurnResult> OfferStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            string text = "Вы нашли номер Римуса и позвонили ему. Что вы хотите ему предложить?";
            return await Cutbacks.Prompt(stepContext, cancellationToken, text, "Совместную работу");
        }

        private async Task<DialogTurnResult> GoingMarshallSecondDialogStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            return await stepContext.BeginDialogAsync(nameof(MarshallSecondDialog), null, cancellationToken);
        }
    }
}