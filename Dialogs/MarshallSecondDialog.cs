﻿using System.Threading;
using System.Threading.Tasks;
using EchoBot.LoopDialogs;
using EchoBot.MyCutbacks;
using EchoBot.Profile;
using EchoBot.Resources;
using Microsoft.Bot.Builder.Dialogs;
using System;

namespace EchoBot.Dialogs
{
    public class MarshallSecondDialog : CancelDialog
    {
        public MarshallSecondDialog() : base(nameof(MarshallSecondDialog))
        {
            AddDialog(new FinalDialog());
            AddDialog(new MarshallAndGov());
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                TakeOfferStepAsync,
                WaitForBenStepAsync,
                MurderStepAsync,
                GiveInventionStepAsync,
                MainPointStepAsync,
                AdministrationStepAsync,
                GoingFinalDialog
            }));
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> TakeOfferStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            await Cutbacks.SendText(Replicas.list[11], stepContext, cancellationToken);
            return await Cutbacks.Prompt(stepContext, cancellationToken,
                "Что ответить?", "Согласиться", "Отказаться");
        }

        private async Task<DialogTurnResult> WaitForBenStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("agree", stepContext);
            CheckLog.AddKey(stepContext.Values);
            if (Cutbacks.Equals("agree", "Отказаться", stepContext))
                return await stepContext.EndDialogAsync(new UserProfile {ResultText = "Отказаться"}, cancellationToken);
            await Cutbacks.SendText(Replicas.list[12], stepContext, cancellationToken);
            await Cutbacks.SendText(Replicas.list[13], stepContext, cancellationToken);
            await Cutbacks.SendImages(ImageURLs.list[6], stepContext, cancellationToken);
            return await Cutbacks.Prompt(stepContext, cancellationToken, "Что делать?",
                "Терпеть Бэна", "Обучить Элу");
        }

        private async Task<DialogTurnResult> MurderStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("wait", stepContext);
            CheckLog.AddKey(stepContext.Values);
            if (Cutbacks.Equals("wait", "Терпеть Бэна", stepContext))
                return await stepContext.EndDialogAsync(new UserProfile {ResultText = "Терпеть"}, cancellationToken);
            await Cutbacks.SendText(Replicas.list[14], stepContext, cancellationToken);
            await Cutbacks.SendImages(ImageURLs.list[7], stepContext, cancellationToken);
            string text = $"Как расправиться с Бэном?{Environment.NewLine}{Environment.NewLine}" +
                          $"1. Убить только Бэна{Environment.NewLine}{Environment.NewLine}" +
                          $"2. Убить Бэна и его семью";
            return await Cutbacks.Prompt(stepContext, cancellationToken, text, "Бэна", "Бэна и его семью");
        }

        private async Task<DialogTurnResult> GiveInventionStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("kill", stepContext);
            CheckLog.AddKey(stepContext.Values);
            await Cutbacks.SendText($"Киборг врывается в дом Колсонов, убивая {stepContext.Values["kill"]}.",
                stepContext, cancellationToken);
            await Cutbacks.SendText(Replicas.list[15], stepContext, cancellationToken);
            return await stepContext.BeginDialogAsync(nameof(MarshallAndGov), null, cancellationToken);
        }

        private async Task<DialogTurnResult> MainPointStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            if (Cutbacks.HasKey("agreenot", "Отказаться"))
                return await stepContext.EndDialogAsync(new UserProfile {ResultText = "Категорически отказаться"},
                    cancellationToken);
            await Cutbacks.SendText(Replicas.list[17], stepContext, cancellationToken);
            return await Cutbacks.Prompt(stepContext, cancellationToken, $"Что делать дальше?{Environment.NewLine}{Environment.NewLine}" +
                                                                         $"1. Совершить суицид{Environment.NewLine}{Environment.NewLine}2. Начать Эндпоинт",
                "Суицид", "Эндпоинт");
        }

        private async Task<DialogTurnResult> AdministrationStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("point",stepContext);
            CheckLog.AddKey(stepContext.Values);
            if(Cutbacks.Equals("point","Суицид",stepContext))
                return await stepContext.EndDialogAsync(new UserProfile {ResultText = "Суицид"}, cancellationToken);
            await Cutbacks.SendText(Replicas.list[18], stepContext, cancellationToken);
            await Cutbacks.SendImages(ImageURLs.list[8], stepContext, cancellationToken);
            return await Cutbacks.Prompt(stepContext, cancellationToken, "Провозгласить монархию или собрать новый состав правительства?",
                "Монархия", "Новый состав");
        }

        private async Task<DialogTurnResult> GoingFinalDialog(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("admire",stepContext);
            CheckLog.AddKey(stepContext.Values);
            if (Cutbacks.Equals("admire", "Монархия", stepContext))
                await Cutbacks.SendText("Вы стали монархом", stepContext, cancellationToken);
            else 
                await Cutbacks.SendText("Вы собрали новое тоталитарное правительство", stepContext, cancellationToken);
            await Cutbacks.SendText("Никто из населения даже не пытался свергнуть режим Римуса, так как даже за одну " +
                                    "мысль киборги убивали с максимальной жестокостью.", stepContext, cancellationToken);
            return await stepContext.BeginDialogAsync(nameof(FinalDialog), null, cancellationToken);
        }
    }
}