﻿using System.Threading;
using System.Threading.Tasks;
using EchoBot.MyCutbacks;
using EchoBot.Profile;
using EchoBot.Resources;
using Microsoft.Bot.Builder.Dialogs;

namespace EchoBot.Dialogs
{
    public class FinalDialog : CancelDialog
    {
        public FinalDialog() : base(nameof(FinalDialog))
        {
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                GoingHomeStepAsync,
                FinalChoiceStepAsync
            }));
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> GoingHomeStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            await Cutbacks.SendText("<Переход игры за Джима> «Вау, сколько событий за столь короткий срок… " +
                                    "Все записи обрываются на этом моменте»", stepContext, cancellationToken);
            return await Cutbacks.Prompt(stepContext, cancellationToken, "Что делать дальше? Стоит вернуться в 2020 " +
                                                                         "или же остаться в этом времени?", 
                "Вернуться", "Остаться");
        }
        private async Task<DialogTurnResult> FinalChoiceStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("home", stepContext);
            CheckLog.AddKey(stepContext.Values);
            if (Cutbacks.Equals("home", "Остаться", stepContext))
                return await stepContext.EndDialogAsync(new UserProfile {ResultText = "Остаться"}, cancellationToken);
            await Cutbacks.SendText(Replicas.list[19], stepContext, cancellationToken);
            return await stepContext.EndDialogAsync(new UserProfile {ResultText = "Вернуться"}, cancellationToken);
        }
    }
}