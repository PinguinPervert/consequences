﻿using System.Threading;
using System.Threading.Tasks;
using EchoBot.MyCutbacks;
using EchoBot.Profile;
using EchoBot.Resources;
using Microsoft.Bot.Builder.Dialogs;

namespace EchoBot.Dialogs
{
    public class MarshallFirstDialog : CancelDialog
    {
        public MarshallFirstDialog() : base(nameof(MarshallFirstDialog))
        {
            AddDialog(new BenDialog());
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                CallStepAsync,
                DatePlanStepAsync,
                BehaviourStepAsync,
                GoingBenDialogStepAsync,
            }));
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> CallStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            if (Cutbacks.HasKey("callPost", "Не звонить"))
                return await Cutbacks.Prompt(stepContext, cancellationToken, "Нельзя терпеть", "Позвонить");
            await Cutbacks.SendText(Replicas.list[5], stepContext, cancellationToken);
            await Cutbacks.SendImages(ImageURLs.list[2], stepContext, cancellationToken);
            return await Cutbacks.Prompt(stepContext, cancellationToken, "«Или все-таки не стоит так торопиться?»",
                "Позвонить", "Не звонить");
        }

        private async Task<DialogTurnResult> DatePlanStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("callPost", stepContext);
            CheckLog.AddKey(stepContext.Values);
            if (Cutbacks.Equals("callPost", "Позвонить", stepContext))
            {
                await Cutbacks.SendText(Replicas.list[6], stepContext, cancellationToken);
                return await Cutbacks.Prompt(stepContext, cancellationToken,
                    "Выберете день", "Пятница", "Четверг");
            }
            return await stepContext.ReplaceDialogAsync(nameof(MarshallFirstDialog), null, cancellationToken);
        }

        private async Task<DialogTurnResult> BehaviourStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("planeDate", stepContext);
            CheckLog.AddKey(stepContext.Values);
            string text = $"Наступает {stepContext.Values["planeDate"]}, Джони приезжает в лобораторию Римуса, операторы" +
                          " выставляют свет и включают камеры. Интервью начинается.";
            await Cutbacks.SendText(text, stepContext, cancellationToken);
            await Cutbacks.SendImages(ImageURLs.list[3], stepContext, cancellationToken);
            return await Cutbacks.Prompt(stepContext, cancellationToken, "Выберете как вы будете вести себя во время интервью.", 
                "Спокойно", "Импульсивно");
        }

        private async Task<DialogTurnResult> GoingBenDialogStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("behav", stepContext);
            CheckLog.AddKey(stepContext.Values);
            if (Cutbacks.Equals("behav", "Спокойно", stepContext))
                return await stepContext.EndDialogAsync(new UserProfile {ResultText = "Спокойно"},
                    cancellationToken);
            await Cutbacks.SendText(Replicas.list[7], stepContext, cancellationToken);
            return await stepContext.BeginDialogAsync(nameof(BenDialog), null, cancellationToken);
        }
    }
}