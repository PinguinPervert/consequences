﻿using System.Threading;
using System.Threading.Tasks;
using EchoBot.MyCutbacks;
using EchoBot.Profile;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using System;
using EchoBot.Resources;

namespace EchoBot.Dialogs
{
    public class MainDialog : CancelDialog
    {
        private readonly UserState _userState;

        public MainDialog(UserState userState) : base(nameof(MainDialog))
        {
            _userState = userState;
            AddDialog(new FirstDialog());
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                InitialDialogStepAsync,
                FinalStepAsync
            }));

            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> InitialDialogStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            return await stepContext.BeginDialogAsync(nameof(FirstDialog), null, cancellationToken);
        }

        private async Task<DialogTurnResult> FinalStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            var userInfo = (UserProfile) stepContext.Result;
            if (userInfo is null)
            {
                var accessor2 = _userState.CreateProperty<UserProfile>(nameof(UserProfile));
                await accessor2.SetAsync(stepContext.Context, userInfo, cancellationToken);
                CheckLog.DeleteAll();
                return await stepContext.EndDialogAsync(null, cancellationToken);
            }
            string status = "Ну вот и все. Хочешь заново?";
            if (CheckResults(stepContext, userInfo, cancellationToken).Result)
            {
                await Cutbacks.SendText("Вау, ты прошел(а) до конца!", stepContext, cancellationToken);
                await Cutbacks.SendText(status, stepContext, cancellationToken);
                CheckLog.DeleteAll();
                return await stepContext.EndDialogAsync(null, cancellationToken);
            }
            await Cutbacks.SendText(status, stepContext, cancellationToken);
            var accessor = _userState.CreateProperty<UserProfile>(nameof(UserProfile));
            await accessor.SetAsync(stepContext.Context, userInfo, cancellationToken);
            CheckLog.DeleteAll();
            return await stepContext.EndDialogAsync(null, cancellationToken);
        }

        private async Task<bool> CheckResults(WaterfallStepContext stepContext, UserProfile userProfile,
            CancellationToken cancellationToken)
        {
            switch (userProfile.ResultText)
            {
                case "Спокойно":
                    await Cutbacks.SendText("Вы никого не заинтересовали", stepContext, cancellationToken);
                    await Cutbacks.SendImages(ImageURLs.list[14], stepContext, cancellationToken);
                    return false;
                case "1943":
                    await Cutbacks.SendText("Умер в окопе", stepContext, cancellationToken);
                    await Cutbacks.SendImages(ImageURLs.list[10], stepContext, cancellationToken);
                    return false;
                case "Бежать":
                    await Cutbacks.SendText("Вас убил киборг", stepContext, cancellationToken);
                    await Cutbacks.SendImages(ImageURLs.list[9], stepContext, cancellationToken);
                    return false;
                case "National Geographic":
                    await Cutbacks.SendText("Вы не познакомились с Римусом", stepContext, cancellationToken);
                    await Cutbacks.SendImages(ImageURLs.list[11], stepContext, cancellationToken);
                    return false;
                case "Отказаться":
                    await Cutbacks.SendText("Спец службы, работающие с Бэном, увидели в вас угрозу. Вас устранили",
                        stepContext, cancellationToken);
                    await Cutbacks.SendImages(ImageURLs.list[12], stepContext, cancellationToken);
                    return false;
                case "Терпеть":
                    await Cutbacks.SendText("Бэн задумал убрать Римуса", stepContext, cancellationToken);
                    return false;
                case "Категорически отказаться":
                    await Cutbacks.SendText("Правительство видит в вас угрозу и убивает", stepContext,
                        cancellationToken);
                    await Cutbacks.SendImages(ImageURLs.list[13], stepContext, cancellationToken);
                    return false;
                case "Суицид":
                    await Cutbacks.SendText("Вы совершили суицид, правительство забирает ваши разработки и " +
                                            "творит анархию. Продолжение следует...", stepContext, cancellationToken);
                    return true;
                case "Остаться":
                    await Cutbacks.SendText("Джим решает остаться в 2120 году. Возможно, у него получится изменить ситуацию." +
                                            $"{Environment.NewLine}{Environment.NewLine}Продолжение следует...",
                        stepContext, cancellationToken);
                    await Cutbacks.SendImages(ImageURLs.list[9], stepContext, cancellationToken);
                    return true;
                case "Вернуться":
                    await Cutbacks.SendImages(ImageURLs.list[9], stepContext, cancellationToken);
                    return true;
                default:
                    return false;
            }
        }
    }
}