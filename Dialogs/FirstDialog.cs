﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using EchoBot.MyCutbacks;
using EchoBot.Profile;
using EchoBot.Resources;
using System;

namespace EchoBot.Dialogs
{
    public class FirstDialog : CancelDialog
    {
        public FirstDialog() : base(nameof(FirstDialog))
        {
            AddDialog(new MarshallFirstDialog());
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                MusicStepAsync,
                TakeSubjectStepAsync,
                ChooseDataStepAsync,
                RunAwayStepAsync,
                FindOtherCaseStepAsync,
                GoingMarshallFirstDialog,
            }));
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> MusicStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            if (Cutbacks.HasKey("doing", "Не брать") & !Cutbacks.HasKey("date", "Час назад"))
            {
                await Cutbacks.SendText(
                    "Джим приходит в школу и идет на занятия. День проходит как обычно. Наступает следующий день, " +
                    "Джим едет в автобусе до школы.", stepContext, cancellationToken);
                string text3 = $"Выберете музыку: {Environment.NewLine}1. LP – Lost on You{Environment.NewLine}{Environment.NewLine}2. Bonnie Tyler - Holding Out For A Hero";
                return await Cutbacks.Prompt(stepContext, cancellationToken, text3, "LP", "Bonnie Tyler");
            }
            if (Cutbacks.HasKey("date", "Час назад"))
            {
                await Cutbacks.SendText("Ох! Я снова в автобусе! Вот чумовая вещь! Надо что-нибудь послушать.", stepContext, cancellationToken);
                string text2 = $"Выберете музыку: {Environment.NewLine}1. Queen – Bohemian Rhapsody{Environment.NewLine}{Environment.NewLine}2. AC/DC - Highway to Hell";
                return await Cutbacks.Prompt(stepContext, cancellationToken, text2, "Queen","AC/DC");
            }
            await Cutbacks.SendText(Replicas.list[0], stepContext, cancellationToken);
            string text = $"Выберете музыку:{Environment.NewLine}1. Happy - Pharrell Williams{Environment.NewLine}{Environment.NewLine}" +
                          $"2. In the end - Linkin park";
            return await Cutbacks.Prompt(stepContext, cancellationToken, text, "Happy", "In the end");
        }

        private async Task<DialogTurnResult> TakeSubjectStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("music", stepContext);
            await Cutbacks.SendText($"Окей, послушаю {stepContext.Values["music"]}", stepContext, cancellationToken);
            if (Cutbacks.HasKey("doing", "Не брать") & !Cutbacks.HasKey("date","Час назад"))
            {
                await Cutbacks.SendText(Replicas.list[1], stepContext, cancellationToken);
                string text2 = "Нет, я все-таки должен узнать что это";
                return await Cutbacks.Prompt(stepContext, cancellationToken, text2, "Взять");
            }

            if (Cutbacks.HasKey("date", "Час назад"))
            {
                await Cutbacks.SendText(Replicas.list[1], stepContext, cancellationToken);
                await Cutbacks.SendText("Так, а вот и снова он", stepContext, cancellationToken);
                string text3 = "Взять странный предмет?";
                return await Cutbacks.Prompt(stepContext, cancellationToken, text3, "Взять");
            }
            await Cutbacks.SendText(Replicas.list[1], stepContext, cancellationToken);
            string text = "Взять странный предмет?";
            return await Cutbacks.Prompt(stepContext, cancellationToken, text, "Взять", "Не брать");
        }

        private async Task<DialogTurnResult> ChooseDataStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("doing", stepContext);
            CheckLog.AddKey(stepContext.Values);
            if (Cutbacks.Equals("doing", "Не брать", stepContext))
                return await stepContext.ReplaceDialogAsync(nameof(FirstDialog), null, cancellationToken);
            if (Cutbacks.HasKey("date", "Час назад"))
            {
                string text2 = "Как же круто! Думаю я смогу отправиться куда-нибудь по-дальше.";
                return await Cutbacks.Prompt(stepContext, cancellationToken, text2, "1943", "2120");
            }
            await Cutbacks.SendText(Replicas.list[2], stepContext, cancellationToken);
            return await Cutbacks.Prompt(stepContext, cancellationToken, "Выберете дату", "1943", "2120", "Час назад");
        }

        private async Task<DialogTurnResult> RunAwayStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("date", stepContext);
            CheckLog.AddKey(stepContext.Values);
            if (Cutbacks.Equals("date", "1943", stepContext))
            {
                await Cutbacks.SendText("Почему бы не вернуться в прошлое?", stepContext, cancellationToken);
                return await stepContext.EndDialogAsync(new UserProfile {ResultText = "1943"},
                                 cancellationToken);
            }
            if (Cutbacks.Equals("date", "Час назад", stepContext))
            {
                await Cutbacks.SendText("Попробую пока так", stepContext, cancellationToken);
                return await stepContext.ReplaceDialogAsync(nameof(FirstDialog), null, cancellationToken);
            }
            await Cutbacks.SendText(Replicas.list[3], stepContext, cancellationToken);
            await Cutbacks.SendImages(ImageURLs.list[0], stepContext, cancellationToken);
            string text = "Достаточно криповая обстановка, может стоит свалить в другое место?";
            return await Cutbacks.Prompt(stepContext, cancellationToken, text, "Бежать", "Остаться");
        }

        private async Task<DialogTurnResult> FindOtherCaseStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("run", stepContext);
            CheckLog.AddKey(stepContext.Values);
            if (Cutbacks.Equals("run", "Бежать", stepContext))
                return await stepContext.EndDialogAsync(new UserProfile {ResultText = "Бежать"},
                    cancellationToken);
            string text = "Джим замечает, что на соседнем столе лежат еще две папки по этому делу.";
            await Cutbacks.SendText(text, stepContext, cancellationToken);
            await Cutbacks.SendImages(ImageURLs.list[1], stepContext, cancellationToken);
            return await Cutbacks.Prompt(stepContext, cancellationToken, "Думаю стоит осмотреть их", "Осмотреть");
        }

        private async Task<DialogTurnResult> GoingMarshallFirstDialog(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("look", stepContext);
            CheckLog.AddKey(stepContext.Values);
            string text = "Джим осматривает все полки и пытается сложить все в единую историю. Вам необходимо ему помочь.";
            await Cutbacks.SendText(text, stepContext, cancellationToken);
            await Cutbacks.SendText(Replicas.list[4], stepContext, cancellationToken);
            return await stepContext.BeginDialogAsync(nameof(MarshallFirstDialog), null, cancellationToken);
        }
    }
}