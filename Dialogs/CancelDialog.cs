﻿using System.Threading;
using System.Threading.Tasks;
using EchoBot.MyCutbacks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;

namespace EchoBot.Dialogs
{
    public class CancelDialog : ComponentDialog
    {
        private const string ResetMsgText = "Reseting...";

        public CancelDialog(string id)
            : base(id)
        {
        }

        protected override async Task<DialogTurnResult> OnContinueDialogAsync(DialogContext innerDc, CancellationToken cancellationToken = default)
        {
            var result = await InterruptAsync(innerDc, cancellationToken);
            if (result != null)
            {
                return result;
            }

            return await base.OnContinueDialogAsync(innerDc, cancellationToken);
        }

        private async Task<DialogTurnResult> InterruptAsync(DialogContext innerDc, CancellationToken cancellationToken)
        {
            if (innerDc.Context.Activity.Type == ActivityTypes.Message)
            {
                var text = innerDc.Context.Activity.Text.ToLowerInvariant();

                switch (text)
                {
                    case "/reset":
                        var cancelMessage = MessageFactory.Text(ResetMsgText, ResetMsgText, InputHints.IgnoringInput);
                        var nextMessage = MessageFactory.Text("Напишите что-нибудь для продолжения");
                        await innerDc.Context.SendActivityAsync(cancelMessage, cancellationToken);
                        await innerDc.Context.SendActivityAsync(nextMessage,cancellationToken);
                        CheckLog.DeleteAll();
                        return await innerDc.CancelAllDialogsAsync(cancellationToken);
                }
            }
            return null;
        }
    }
}