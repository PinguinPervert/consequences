﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;

namespace EchoBot.Bots
{
    public class WelcomeConsBot<T> : Consequences<T> where T : Dialog
    {
        public WelcomeConsBot(ConversationState conversationState, UserState userState, T dialog,ILogger<Consequences<T>> logger) : base(
            conversationState, userState, dialog,logger)
        {
        }

        protected override async Task OnMembersAddedAsync(IList<ChannelAccount> membersAdded,
            ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            foreach (var member in membersAdded)
            {
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    var reply = MessageFactory.Text(
                        $"Привет, незнакомец. Это квест-бот Consequences. В нем ты можешь управлять ходом истории и " +
                        "вершить судьбы. Если у тебя получится пройти хотя бы половину, то можешь задуматься о том, " +
                        "чтобы дойти до конца. Тем более тебя ждут целых 3 концовки и куча вариантов смерти. Так же " +
                        "тебе придется играть за нескольких героев. Ну что, ты готов? В таком случае напиши что-нибудь.");
                    await turnContext.SendActivityAsync(reply, cancellationToken);
                }
            }
        }
    }
}