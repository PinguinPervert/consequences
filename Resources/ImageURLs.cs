﻿using System.Collections.Generic;

namespace EchoBot.Resources
{
    public static class ImageURLs
    {
        public static List<string> list = new List<string>()
        {
            "https://avatars.mds.yandex.net/get-pdb/2374077/84b16f81-7da0-4c33-9d97-a7750313d5b7/s1200?webp=false",
            "http://kingisepp.ru/images/news/block/3708b.jpg",
            "https://c4.wallpaperflare.com/wallpaper/77/154/473/server-room-lights-dark-wallpaper-preview.jpg",
            "https://ief-usfeu.ru/uploads/57d7f604677698a49b872b63f5c111cd.jpg",
            "https://hsto.org/webt/e1/zc/el/e1zcelly5guitf8br1m9uz8fyaq.jpeg",
            "https://img01.lachschon.de/images/215531_Wissarionowitsch_zvt3jWp.jpg",
            "https://www.vladtime.ru/uploads/posts/2015-12/1450343258_1.jpg",
            "https://phototass4.cdnvideo.ru/width/1020_b9261fa1/tass/m2/uploads/i/20181109/4860573.jpg",
            "https://eaibicho.com/upload/publication/5bad870f41531b2f803e69df/cover/perguntas-portuguese.jpg",
            "https://sun9-63.userapi.com/c637923/v637923995/498df/w-bT2yRElkU.jpg",
            "http://cdn-frm-eu.wargaming.net/wot/ru/uploads/monthly_11_2018/post-1185722-0-68437600-1542273533.jpg",
            "https://wallpapers-fenix.eu/lar/181207/043033289.jpg",
            "https://pbs.twimg.com/media/Dm-_eehXgAEqdO3.jpg",
            "http://afflictor.com/wp-content/uploads/2015/02/afp-e-faces-12-clintons-e1422771066593.jpg",
            "https://1gr.cz/fotky/bulvar/16/023/cl6/ANN619bca_profimedia_0126503175.jpg"
        };
    }
}