﻿using System.Threading;
using System.Threading.Tasks;
using EchoBot.MyCutbacks;
using EchoBot.Resources;
using Microsoft.Bot.Builder.Dialogs;
using System;
using EchoBot.Dialogs;

namespace EchoBot.LoopDialogs
{
    public class MarshallAndGov : CancelDialog
    {
        public MarshallAndGov() : base(nameof(MarshallAndGov))
        {
            AddDialog(new GiveDialog());
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                GiveInventStepAsync,
                DestinationPointStepAsync
            }));
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> GiveInventStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            if (Cutbacks.HasKey("agreenot", "Доработать"))
                return await Cutbacks.Prompt(stepContext, cancellationToken,
                    "Проходит время. Отдать технологию в спец. отдел?", "Отдать", "Отказаться");
            await Cutbacks.SendText(
                "Проходит месяц. Руководство проекта уже требует от Римуса результатов и скорого внедрения.",
                stepContext, cancellationToken);
            string text = $"Отдать технологию в спец. отдел?{Environment.NewLine}{Environment.NewLine}" +
                          "1. Отдать" +
                          $"{Environment.NewLine}{Environment.NewLine}" +
                          $"2. Оставить на доработку{Environment.NewLine}" +
                          "3. Категорически отказаться";
            return await Cutbacks.Prompt(stepContext, cancellationToken, text,"Отдать", "Доработать", "Отказаться" );
        }

        private async Task<DialogTurnResult> DestinationPointStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("agreenot",stepContext);
            CheckLog.AddKey(stepContext.Values);
            if(Cutbacks.Equals("agreenot","Отказаться",stepContext))
                return await stepContext.EndDialogAsync(stepContext.Values["agreenot"], cancellationToken);
            if(Cutbacks.Equals("agreenot","Доработать",stepContext))
                return await stepContext.ReplaceDialogAsync(nameof(MarshallAndGov), null, cancellationToken);
            await Cutbacks.SendText(Replicas.list[16], stepContext, cancellationToken);
            return await stepContext.BeginDialogAsync(nameof(GiveDialog),null,cancellationToken);
        }
    }
}