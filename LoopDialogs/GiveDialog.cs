﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using EchoBot.Dialogs;
using EchoBot.MyCutbacks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;

namespace EchoBot.LoopDialogs
{
    public class GiveDialog : CancelDialog
    {
        public GiveDialog() : base(nameof(GiveDialog))
        {
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                StructStepAsync,
                ContinueStepAsync
            }));
            InitialDialogId = nameof(WaterfallDialog);
        }

        private static List<string> list = new List<string>() {"Полиция", "МЧС", "Охрана"};
        private static List<string> list2 = new List<string>() {"Полиция", "МЧС", "Охрана"};

        private async Task<DialogTurnResult> StructStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(ChoicePrompt), new PromptOptions
            {
                Prompt = MessageFactory.Text("Куда еще внедрить разработку?"),
                Choices = ChoiceFactory.ToChoices(list)
            }, cancellationToken);
        }

        private async Task<DialogTurnResult> ContinueStepAsync(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("cont", stepContext);
            list.Remove(stepContext.Values["cont"].ToString());
            if (list.Count > 0)
                return await stepContext.ReplaceDialogAsync(nameof(GiveDialog), list, cancellationToken);
            list.AddRange(list2);
            CheckLog.AddKey(stepContext.Values);
            await Cutbacks.SendText("Римус предлагает внедрить киборгов в полицию, МЧС, ФСО, но с уникальной настройкой" +
                                    " для каждой организации.", stepContext, cancellationToken);
            return await stepContext.EndDialogAsync(stepContext.Values["cont"], cancellationToken);
        }
    }
}