﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using EchoBot.Dialogs;
using EchoBot.MyCutbacks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;

namespace EchoBot.LoopDialogs
{
    public class CallDialog : CancelDialog
    {
        private static List<string> numbers = new List<string>() {"Стюарту", "Бэннингтону", "Блэку"};
        private static List<string> numbers2 = new List<string>() {"Стюарту", "Бэннингтону", "Блэку"};

        public CallDialog() : base(nameof(CallDialog))
        {
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                Call,
                ContinueCall
            }));
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> Call(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            await Cutbacks.SendText("В вашей записной книжке есть номера Майкла Стюарта - ректора MIT, " +
                                    "Ричарда Бэннингтона - брата Римуса и " +
                                    "Джорджа Блэка - предсежателя окружного отделения полиции.",
                stepContext, cancellationToken);
            string text = "Кому вы хотите позвонить?";
            //return await Cutbacks.Prompt(stepContext, cancellationToken, text, numbers.ToArray());
            return await stepContext.PromptAsync(nameof(ChoicePrompt), new PromptOptions
            {
                Prompt = MessageFactory.Text(text),
                Choices = ChoiceFactory.ToChoices(numbers)
            }, cancellationToken);
        }

        private async Task<DialogTurnResult> ContinueCall(WaterfallStepContext stepContext,
            CancellationToken cancellationToken)
        {
            Cutbacks.FoundChoice("call", stepContext);
            if (!Cutbacks.Equals("call", "Бэннингтону", stepContext))
            {
                numbers.Remove(stepContext.Values["call"].ToString());
                string person = stepContext.Values["call"].ToString();
                await Cutbacks.SendText($"{person.Remove(person.Length - 1)} не знает номера Римуса.", stepContext, cancellationToken);
                return await stepContext.ReplaceDialogAsync(nameof(CallDialog), numbers, cancellationToken);
            }
            numbers.Clear();
            numbers.AddRange(numbers2);
            return await stepContext.EndDialogAsync(stepContext.Values["call"], cancellationToken);
        }
    }
}