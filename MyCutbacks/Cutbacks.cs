﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;

namespace EchoBot.MyCutbacks
{
    public static class Cutbacks
    {
        public static async Task<DialogTurnResult> Prompt(WaterfallStepContext stepContext,
            CancellationToken cancellationToken, string text, params string[] list)
        {
            return await stepContext.PromptAsync(nameof(ChoicePrompt), new PromptOptions
            {
                Prompt = MessageFactory.Text(text),
                Choices = ChoiceFactory.ToChoices(list.ToList())
            }, cancellationToken);
        }
        public static async Task Typing(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            await stepContext.Context.SendActivitiesAsync(
                new IActivity[]
                {
                    new Activity {Type = ActivityTypes.Typing},
                    new Activity {Type = "delay", Value = 500},
                }, cancellationToken);
        }

        public static async Task SendText(string text, WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            await stepContext.Context.SendActivityAsync(text, null, null, cancellationToken);
        }

        public static void FoundChoice(string key, WaterfallStepContext stepContext)
        {
            stepContext.Values[key] = ((FoundChoice) stepContext.Result).Value;
        }

        public static bool HasKey(string key, string result)
        {
            return CheckLog.HasKey(new KeyValuePair<string, object>(key, result));
        }
        public static bool Equals(string key, string result, WaterfallStepContext stepContext)
        {
            return ReferenceEquals(stepContext.Values[key], result);
        }
        public static async Task SendImages(string url, WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var reply = stepContext.Context.Activity.CreateReply();
            var att = new Attachment
            {
                ContentUrl = url,
                ContentType = "image/jpg",
                Name = " "
            };
            reply.Attachments = new List<Attachment>() {att};
            await stepContext.Context.SendActivityAsync(reply, cancellationToken);
        }
    }
}