﻿using System.Collections.Generic;
using System.Linq;

namespace EchoBot.MyCutbacks
{
    public static class CheckLog
    {
        private static List<KeyValuePair<string, object>> _list = new List<KeyValuePair<string, object>>();
        public static void AddKey(IDictionary<string,object> dictionary)
        {
            if(dictionary != null)
                _list.Add(dictionary.Last());
        }
        public static bool HasKey(KeyValuePair<string,object> pair)
        {
            return _list != null && _list.Contains(pair);
        }
        public static void DeleteAll()
        {
            _list.Clear();
        }
        public static bool HasLast(KeyValuePair<string, object> pair)
        {
            if(_list.Count > 0)
                return _list.Last().Key == pair.Key & _list.Last().Value == pair.Value;
            return false;
        }
    }
}